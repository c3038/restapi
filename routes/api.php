<?php

use App\Http\Controllers\API\RandomNumber\NumberController;
use App\Http\Controllers\API\RandomNumber\ShowController;
use App\Http\Controllers\API\RandomNumber\StoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/number', [NumberController::class, 'generate']);
Route::get('/number/{number}', [NumberController::class, 'retrieve']);
