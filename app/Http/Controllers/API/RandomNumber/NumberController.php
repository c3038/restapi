<?php

namespace App\Http\Controllers\API\RandomNumber;

use App\Http\Controllers\API\RandomNumber\BaseController;
use App\Http\Resources\RandomNumber\RandomNumberResource;
use App\Models\Number;

class NumberController extends BaseController
{
   public function retrieve(int $id)
   {
      $number  = Number::find($id);
      if (is_null($number)) {
         return  $this->sendError('Numbert does not exist.');
      }
      return $this->sendResponse(new RandomNumberResource($number), 'Number fetched.');
   }

   public function generate()
   {
      $number = $this->service->store();

      if (!($number instanceof Number)) {
         return $this->sendError($number);
      }
      return $this->sendResponse(new RandomNumberResource($number), 'Number created.');
   }
}
