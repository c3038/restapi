<?php

namespace App\Console\Commands;

use App\Http\Controllers\API\RandomNumber\NumberController;
use App\Http\Controllers\API\RandomNumber\StoreController;
use App\Service\RandomNumber\Service;
use Illuminate\Console\Command;

class CreateRandNumberCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'random:number';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new random number';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $number = new NumberController(new Service());
        $this->info($number->generate());
    }
}
