<?php

namespace App\Console\Commands;

use App\Http\Controllers\API\RandomNumber\NumberController;
use App\Service\RandomNumber\Service;
use Illuminate\Console\Command;

class FindNumberByIdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'findnumber:byid {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find number by id';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $number = new NumberController(new Service());

        $id = (int)$this->argument('id');
        $this->info($number->retrieve($id));
    }
}
