<?php

namespace App\Service\RandomNumber;

use App\Models\Number;
use Illuminate\Support\Facades\DB;

class Service
{
   public function store()
   {

      try {
         DB::beginTransaction();
         $number =  Number::create([
            'number' => random_int(0, 70000),
         ]);
         DB::commit();
      } catch (\Exception $exception) {
         DB::rollBack();
         return $exception->getMessage();
      }

      return $number;
   }
}
